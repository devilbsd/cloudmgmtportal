# README #

Cloud Management Portal is a Proof of Concept for using 
Check Point's Capsule Cloud public API.

It is designed as a dashboard for a reseller.
Customer accounts will be created as a group, users from the same company
will be added to the same group and apply the same policy to all users on the
same company.

This portal allows you to:
* Create and delete accounts and users.
* Automatically send enrolment code to users.
* List and Edit existing accounts and users.
* List all available applications to be filtered.


WARINING: At this state the code is a mess...


### Online Version ###
[capsulepoc.fusiondementes.net](capsulepoc.fusiondementes.net)

### About ###
Cloud Management Portal by 
Javier Reyna Padilla: jpadilla@checkpoint.com