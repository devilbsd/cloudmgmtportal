import cherrypy
from cherrypy.lib.static import serve_file
from Cheetah.Template import Template
import json, os, requests

class Root:
    # -- HTML Templates
    msdk = Template(file="msdk/templates/index.html")
    settings = Template(file="msdk/templates/settings.html")
    accounts = Template(file="msdk/templates/accounts.html")	
    services = Template(file="msdk/templates/services.html")
    about = Template(file="msdk/templates/about.html")
    showapp = Template(file="msdk/templates/showapps.html")
    showcats = Template(file="msdk/templates/showcatgs.html")
    usercreated = Template(file="msdk/templates/usercreated.html")
    cloud_url = 'https://cloud.checkpoint.com/cp-cloud-api.php?format=json'
    
    @cherrypy.expose
    def index(self, section = 'settings', view = None):
        if section == 'policies':
            self.msdk.content = str(self.policies)
        
        elif section == 'services':
            self.msdk.content = str(self.services)

        elif section == 'about':
            self.msdk.content = str(self.about)
        
        elif section == 'accounts':
            self.accounts.view = view
            self.accounts.msgCode = '00'
            if view == 'listAccounts':
                self.accounts.accountlist = self.getAccountList()                
            elif view == 'listUsers':    
                self.accounts.userlist = self.getUserList()
                #self.accounts.apiKey = self.getapiKey()
            self.msdk.content = str(self.accounts)

        elif section == 'logs':
            self.msdk.content = str(self.about)

        else:
            self.settings.email = self.getSettings()['LoginData']['email']
            self.settings.return_code = '00'
            self.msdk.content = str(self.settings)
        return str(self.msdk)
    
    @cherrypy.expose
    def saveConfig(self, email=None, password=None):
        with open('capsule.conf', 'w') as capsuleConf:
            loginData = {'LoginData':{'email': email,'password':password}}
            json.dump(loginData, capsuleConf)
            self.settings.return_code = '01'
            self.msdk.content = str(self.settings)
            return str(self.msdk)
        
    def getSettings(self):
        '''Fetch Capsucloud Login Data'''
        with open('capsule.conf', 'r') as capsuleConf:
            config = json.load(capsuleConf)
            return config
        
    def getapiKey(self):
        LoginData = self.getSettings()['LoginData']
        command = json.dumps({'command': 'register',
                                       'LoginData':LoginData})
        req = requests.post(self.cloud_url, data = command)
        return json.loads(req.text)['apiKey']

    
    def getUserList(self):
        '''Get list of users
            REVIEW THIS FUNCTION, SHOULD BE PART OF ALL ACCOUNTS'''
        self.accounts.accountList = self.sendApiCmd(apiCmd = 'getUserList')['data']['results']
        self.msdk.content = str(self.accounts)
        return str(self.msdk)
    
    def getAccountList(self):
        '''Get list of groups'''
        self.accounts.accountList = self.sendApiCmd(apiCmd = 'getGroupsList')['data']['results']
        self.msdk.content = str(self.accounts)
        return str(self.msdk) 
    
    @cherrypy.expose
    def createUser(self, userName=None, userEmail=None, account=None):
        apiKey = self.getapiKey()
        email = self.getSettings()['LoginData']['email']
        password = self.getSettings()['LoginData']['password']
        command = json.dumps({'command':'createUser',
                              'params':{'userName':userName,
                                        'userEmail':userEmail,
                                        'sendRegistrationEmail':'true'},
                               'LoginData':{'email':email,
                                            'password':password,
                                            'apiKey':apiKey}})
        
        req = requests.post(self.cloud_url, data = command)
        createUserResponse = json.loads(req.text)
        print createUserResponse
        self.usercreated.userName = userName
        self.usercreated.userEmail = userEmail
        self.usercreated.otp = createUserResponse['data']['otp']
        self.msdk.content = str(self.usercreated)
        return str(self.msdk)
    
    @cherrypy.expose
    def createAccount(self, accountName=None, policy = None, accessExpire=None):
        '''An acccount for CMP is a group on Capsule Cloud, user part of a group will be
        members of the same group.
        If groups are implemented on CMP context those will be recognized as groups
        for CMP but not on the CC management.'''
        params = {}
        params['groupName'] = accountName
        #params['groupDescription'] = 'Created by CMP'
        self.usercreated.requestResponse = self.sendApiCmd(apiCmd = 'createGroup', params = params)['data']['results']
        self.msdk.content = str(self.usercreated)
        return str(self.msdk)
    
        # self.usercreated.userName = accountName
        # self.usercreated.userEmail = None
        # self.usercreated.otp = None
        # self.msdk.content = str(self.usercreated)
        
    @cherrypy.expose
    def deleteUser(self, userID=None):
        apiKey = self.getapiKey()
        email = self.getSettings()['LoginData']['email']
        password = self.getSettings()['LoginData']['password']
        command = json.dumps({'command':'deleteUser',
                              'params':{'userID':userID},
                               'LoginData':{'email':email,
                                            'password':password,
                                            'apiKey':apiKey}})
        req = requests.post(self.cloud_url, data = command)
        deleteUserResponse = json.loads(req.text)
        print deleteUserResponse
        self.accounts.msgCode = '03' 
        self.accounts.userlist = self.getUserList()
        self.accounts.apiKey = self.getapiKey()
        self.msdk.content = str(self.accounts)
        return str(self.msdk)
    
    @cherrypy.expose
    def showapps(self):
        #requestResponse = self.sendApiCmd(apiCmd = 'policyGetApplicationList')
        self.showapp.requestResponse = self.sendApiCmd(apiCmd = 'policyGetApplicationList')['data']['results']
        self.msdk.content = str(self.showapp)
        return str(self.msdk)
    
    @cherrypy.expose
    def showcatgs(self):
        self.showcats.requestResponse = self.sendApiCmd(apiCmd = 'policyGetCategoryList')['data']['results']
        self.msdk.content = str(self.showcats)
        return str(self.msdk)
    
    @cherrypy.expose
    def sendApiCmd(self, apiCmd = None, params = None ):
        '''Construct API Command and send it to the cloud.
            getSettings() is not a api call but  will fetch login data from capsulecloud.conf '''
        apiRequest = {'command':apiCmd,
                      'LoginData':{'email':self.getSettings()['LoginData']['email'],
                                   'password':self.getSettings()['LoginData']['password'],
                                   'apiKey':self.getapiKey()}}
        if params   :
            apiRequest['params'] = params
        print '\n ==> API Request: ', apiRequest
        req = requests.post(self.cloud_url, data = json.dumps(apiRequest))
        requestResponse = json.loads(req.text)
        print '\n ==> API Response: ', requestResponse
        return requestResponse 

if __name__ == '__main__':
    current_dir = os.path.dirname(os.path.abspath(__file__))
    cherrypy.config.update({'environment': 'production',
                            'log.screen': False,
                            'log.error_file': os.path.join(current_dir,'/var/log/capsulecloudpoc.log'),
                            'server.socket_host': '192.168.206.128',
                            'server.socket_port': 2431})

    conf = {'/images' : {'tools.staticdir.on': True,
                         'tools.staticdir.dir': os.path.join(current_dir,'msdk/images'),
                         'tools.staticdir.content_types':{'jpg':'image/jpeg',
                                                          'png':'image/png',
                                                          'gif':'image/gif'}},
            '/Style.css' : {'tools.staticfile.on': True,
                            'tools.staticfile.filename': os.path.join(current_dir,'msdk/Style.css')}}
    
    cherrypy.quickstart(Root(), '/', config=conf)
